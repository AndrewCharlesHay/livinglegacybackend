
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('posts').del()
    .then(function () {
      // Inserts seed entries
      return knex('posts').insert([
        {user: '1', deceased: 1, comment: 'Hi', img: 'https://boygeniusreport.files.wordpress.com/2016/11/puppy-dog.jpg?quality=98&strip=all&w=782', approved: false},
        {user: '1', deceased: 2, comment: 'Hello The eighth child of the Jackson family, Jackson made his professional debut in 1964 with his elder brothers Jackie, Tito, Jermaine, and Marlon as a member of the Jackson 5. Jackson began his solo career in 1971 while at Motown Records, and rose to solo stardom with his fifth studio album Off the Wall (1979). By the early 1980s, Jackson became a dominant figure in popular music. His music videos, including those for "Beat It", "Billie Jean", and "Thriller" from his sixth studio album Thriller (1982), are credited with breaking racial barriers and transforming the medium into an art form and promotional tool. Their popularity helped propel the television channel MTV into a significant factor of 1980s pop culture. Jackson continued to innovate with videos such as "Leave Me Alone", "Smooth Criminal", "Black or White", "Remember the Time" and "Scream", and forged a reputation as a global touring artist. With songs such as "Man in the Mirror", "Black or White", Heal the World, "Earth Song" and "They Don’t Care About Us", Jacksons music emphasized racial integration, environmentalism and fighting against prejudice and injustice. Through stage and video performances, he popularized complicated dance techniques such as the moonwalk, to which he gave the name. His sound and style have influenced artists of various genres.', img: 'https://www.guidedogs.org/wp-content/uploads/2019/11/website-donate-mobile.jpg', approved: true},
        {user: '3', deceased: 2, comment: 'He was brave, Michael Joseph Jackson was an American singer, songwriter, and dancer. Dubbed the "King of Pop", he is regarded as one of the most significant cultural figures of the 20th century and one of the greatest entertainers in the history of music.', img: '', approved: true},
        {user: '2', deceased: 2, comment: 'Hi There', img: 'https://www.humanesociety.org/sites/default/files/styles/1240x698/public/2019/02/dog-451643.jpg?h=bf654dbc&itok=MQGvBmuo', approved: true}
      ]);
    });
};
