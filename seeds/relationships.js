
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('relationships').del()
    .then(function () {
      // Inserts seed entries
      return knex('relationships').insert([
        {id: "fd43e4e6-aca5-45fe-a2a5-135b2b48a6a9", user: '1', deceased: 2, relationship: 1, approved: true},
        {id: "4447fc59-4197-422a-9cce-026f7266886f", user: '2', deceased: 1, relationship: 3, approved: false},
        {id: "f4221b59-a844-42d1-90c3-679f27a33cc4", user: '3', deceased: 3, relationship: 2, approved: true}
      ]);
    });
};
