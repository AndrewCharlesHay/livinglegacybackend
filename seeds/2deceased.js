exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('deceased').del()
    .then(function () {
      // Inserts seed entries
      return knex('deceased').insert([
        {id: 1, fname: 'Michael', lname: 'Jackson', img: '', obituary: 'He sang and touched us all', cemetary: 1},
        {id: 2, fname: 'Billy', lname: 'Maze', img: 'https://pbs.twimg.com/profile_images/830269594749251585/UdkUxOl__400x400.jpg', obituary: 'He saved our clothes but could not save himself', cemetary: 1},
        {id: 3, fname: 'Kirk', lname: 'Kobain', img: '', obituary: 'His body smells like teen spirit', cemetary: 1}
      ]);
    });
};
