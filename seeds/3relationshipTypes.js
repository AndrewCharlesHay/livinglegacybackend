
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('relationship_types').del()
    .then(function () {
      // Inserts seed entries
      return knex('relationship_types').insert([
        {id: 1, description: 'Friend'},
        {id: 2, description: 'Sister'},
        {id: 3, description: 'Brother'},
      ]);
    });
};
