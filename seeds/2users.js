
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {id: '1', fname: 'Andy', lname: 'Hay', email: 'andrewcharleshay96@gmail.com', img: 'https://images.unsplash.com/photo-1516467508483-a7212febe31a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80', admin: true},
        {id: '2', fname: 'John', lname: 'Robbins', email: 'JohnRobbins@gmail.com', img: 'https://www.greenbiz.com/sites/default/files/styles/gbz_article_primary_breakpoints_kalapicture_screen-md_1x/public/images/articles/featured/pigfarming.jpg?itok=eEBMnk4s&timestamp=1547925853', admin: true},
        {id: '3', fname: 'Buddy', lname: 'Hay', email: 'I am a dog', img: 'https://wi-images.condecdn.net/image/jjAAGJWy79x/crop/1620/f/pigbrain.jpg', admin: false}
      ]);
    });
};
