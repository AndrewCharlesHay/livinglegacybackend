
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('cemetaries').del()
    .then(function () {
      // Inserts seed entries
      return knex('cemetaries').insert([
        {id: 1, name: "Forest Lawn", lat: 34.1246267, long: -118.2511632, city: "Glendale", state: "California", country: "United States"}
      ]);
    });
};
