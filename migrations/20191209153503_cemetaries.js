
exports.up = function(knex) {
    return knex.schema.createTable('cemetaries', function (t) {
        t.increments();
        t.text('name')
        t.float('lat');
        t.float('long');
        t.text('city');
        t.text('state');
        t.text('country');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('cemetaries')
};
