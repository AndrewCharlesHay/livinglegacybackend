
exports.up = function(knex) {
    return knex.schema.createTable('purchases', function (t) {
        t.increments();
        t.integer('deceased')
        .references('deceased.id')
        t.text('purchaser')
        .references('users.id')
        t.integer('itemType')
        .references('items.id')
        t.float('purchasePrice')
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('purchases')
};
