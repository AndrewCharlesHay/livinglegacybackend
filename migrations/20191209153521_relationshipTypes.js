
exports.up = function(knex) {
    return knex.schema.createTable('relationship_types', function (t) {
        t.increments();
        t.text('description')
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('relationship_types')
};
