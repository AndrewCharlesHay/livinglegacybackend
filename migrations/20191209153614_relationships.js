
exports.up = function(knex) {
    return knex.schema.createTable('relationships', function (t) {
        t.uuid('id').primary();
        t.text('user')
        .references('users.id')
        t.integer('deceased')
        .references('deceased.id')
        t.integer('relationship')
        .references('relationship_types.id')
        t.boolean('approved')
    });
  
};

exports.down = function(knex) {
    return knex.schema.dropTable('relationships')
};
