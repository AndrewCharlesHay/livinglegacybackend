
exports.up = function (knex) {
    return knex.schema.createTable('users', function (t) {
        t.text('id').primary();
        t.text('fname').notNull().defaultTo('');
        t.text('lname').notNull().defaultTo('');
        t.text('email').notNull().defaultTo('');
        t.text('img')
        t.boolean('admin');
        t.timestamp('created_at').defaultTo(knex.fn.now())
        t.timestamp('updated_at').defaultTo(knex.fn.now())
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('users')
};
