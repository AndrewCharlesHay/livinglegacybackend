
exports.up = function(knex) {
    return knex.schema.createTable('items', function (t) {
        t.increments();
        t.text('description')
        t.text('imgURL')
        t.float('price')
    });
};

exports.down = function(knex) {
  
    return knex.schema.dropTable('items')
};
