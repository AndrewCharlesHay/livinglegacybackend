
exports.up = function(knex) {
    return knex.schema.createTable('roles', function (t) {
        t.increments();
        t.text('description')
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('roles')
};
