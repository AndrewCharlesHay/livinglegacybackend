
exports.up = function (knex) {
    return knex.schema.createTable('posts', function (t) {
        t.increments('id').primary();
        t.text('user')
        .references('users.id')
        t.integer('deceased')
        .references('deceased.id')
        t.text('comment')
        t.text('img')
        t.boolean('approved').notNullable().defaultsTo(false)
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('posts')
};
