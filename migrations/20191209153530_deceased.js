
exports.up = function(knex) {
    return knex.schema.createTable('deceased', function (t) {
        t.increments('id').primary();
        t.text('fname').notNull().defaultsTo('');
        t.text('lname').notNull().defaultsTo('');
        t.integer('cemetary')
        .references('cemetaries.id')
        t.text('img').notNull().defaultTo('https://journeypurebowlinggreen.com/wp-content/uploads/2018/05/placeholder-person.jpg')
        t.text('obituary').notNull().defaultTo('obituary here');
        t.timestamp('created_at').defaultTo(knex.fn.now())
        t.timestamp('updated_at').defaultTo(knex.fn.now())
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('deceased')
};
