

module.exports = {

  development: {
    client: 'pg',
    connection: 'postgres://localhost:5432/lldb'
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'lldb',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'lldb',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};