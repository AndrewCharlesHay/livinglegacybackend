const express = require('express');
const router = express.Router();
const mw = require('./middleware');

router.get('/:id', function (req, res, next) {
    const deceased = req.params.id;
    knex('posts').select()
        .where('deceased', deceased)
        .andWhere('approved', true)
        .then(async posts => {
            if (posts.length) {
                for (let i = 0; i < posts.length; i++) {
                    let post = posts[i];
                    await knex('users').first()
                        .where('id', post.user)
                        .then(async user => {
                            console.log(user)
                            await knex('relationships').first()
                                .where('user', user.id)
                                .then(async relationship => {
                                    console.log(`For ${i} relationship is ${relationship}`)
                                    await knex('relationship_types').first()
                                        .where('id', relationship.relationship)
                                        .then(relationshipType => {
                                            user.relationship = relationshipType;
                                            post.user = user;
                                            posts[i] = post
                                        })
                                })

                        })
                }
                await res.status(200).json(posts)
            }
            else {
                res.status(400).json({ err: "no posts" })
            }
        })
})

router.post('/:id', async (req, res) => {
    const user = {
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email
    }
    const userId = await mw.getUserByEmail(user)

    const post = {
        deceased: req.params.id,
        comment: req.body.comment,
        img: req.body.img,
        approved: true,
        user: userId
    }

    const relationship = {
        user: userId,
        deceased: req.params.id,
        relationship: req.body.relationship,
        approved: true
    }
    await mw.postRelationship(relationship);
    await knex('posts').insert(post)
        .then(() => res.status(200).json({
            message: `Post Added`
        })
        )
        .catch(err => res.status(400).json({ "error": err }))
})

module.exports = router;
