var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/:id', async function (req, res) {
    const id = req.params.id;
    await knex('deceased').first()
        .where('id', id)
        .then(deceased => {
            res.status(200).send(deceased)
        })
        .catch(err => res.status(400).json(err))
});

router.get('/:fname/:lname', function (req, res, next) {
    const fname = req.params.fname;
    const lname = req.params.lname;
    knex('deceased').select()
        .whereRaw('LOWER(fname) LIKE ?', '%' + fname.toLowerCase() + '%')
        .whereRaw('LOWER(lname) LIKE ?', '%' + lname.toLowerCase() + '%')
        .then(deceased => {
            if (deceased.length = 1) {
                res.status(200).json(deceased[0])
            }
            else {
                res.status(300).json({ "error": "there are more than one people that match the name" })
            }
        })
        .catch(err => res.status(400).json(err))
});

router.post('/', function (req, res) {
    knex('deceased').insert(req.body)
        .then(() => res.status(200).json({
            message: `Deceased Added`
        })
            .catch(err => res.status(400).json({ "error": err })))
})
router.put('/:id', function (req, res) {
    const id = req.params.id;
    knex('deceased')
        .where('id', id)
        .update(req.body)
        .then(() => res.status(200).json({
            message: `Deceased updated`
        })
            .catch(err => res.status(400).json({ "error": err })))
})
router.delete('/:id', function (req, res) {
    const id = req.params.id;
    knex('deceased')
        .where('id', id)
        .del()
        .then(() => {
            res.status(200).json({
                message: 'Deceased Deleted'
            })
        })
        .catch(err => res.status(400).json({ "error": err }))
})
module.exports = router;
