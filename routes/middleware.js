const uuidv1 = require('uuid/v1');

module.exports = {
    getUserByEmail: async function (data) {
        console.log("user: ",data)
        let id = "test"
        await knex('users').first()
            .where('email', data.email)
            .then(async user => {
                if (user !== undefined) {
                    id = user.id;
                }
                else {
                    id = uuidv1();
                    console.log("id: ", id)
                    data.id = id;
                    await this.postUser(data);
                }
            })
        return id;
    },
    postUser: async function (data) {
        let user = {
            fname: data.fname,
            lname: data.lname,
        }
        if (data.id) {
            user.id = data.id;
        }
        else {
        }
        await knex('users').insert(user)
            .then(() => console.log("user added"))
    },
    postRelationship: async function (data) {
        data.id = uuidv1();
        await knex('relationships').insert(data)
        .then(() => console.log("relationship Created: ",data))
    }
}