var express = require('express');
var router = express.Router();
const mw = require('./middleware');
// Get all relationships
router.get('/', function (req, res, next) {
    knex('relationships').select()
    .then(relationships => {
        res.status(200).json(relationships)
    })
    .catch(err => res.status(400).json(err))
})
//Get single relationship
router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    knex('relationships').first()
        .where('id', id)
        .then(relationship => {
            res.status(200).json(relationship)
        })
        .catch(err => res.status(400).json(err))
});
router.get('user/:id', function (req, res, next) {
    knex('relationships').select()
    .where('user', req.body.id)
        .then(relationships => {
            res.status(200).json(relationships)
        })
        .catch(err => res.status(400).json(err))
})
router.post('/:userID', function (req, res) {
    const data = {
        user: req.params.userID,
        deceased: req.body.deceased,
        relationship: req.body.relationship,
        approved: false
    }
    mw.postRelationship(data)
})
router.put('/:id', function(req, res) {
    const data = {}
    if(req.body.userID){
        data.user = req.body.userID
    }
    if(req.body.deceased){
        data.deceased = req.body.deceased;
    }
    if(req.body.relationship){
        data.relationship = req.body.relationship;
    }
    if(req.body.approved !== undefined){
        data.approved = req.body.approved;
    }
    knex('relationships')
    .where('id',req.params.id)
    .update(data)
    .then(() => res.status(200).json({
        message: 'Relationship updated'
    }))
})
router.delete('/:id', function (req, res) {
    const id = req.params.id;
    knex('relationships')
    .where('id', id)
    .del()
    .then(() => res.status(200).json({
        message: 'Relationship deleted'
      }))
})
module.exports = router;