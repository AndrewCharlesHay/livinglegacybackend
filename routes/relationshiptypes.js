var express = require('express');
var router = express.Router();
//Get relationtype from id
router.get('/:id', function (req, res, next) {
    knex('relationship_types').first()
    .where('id', req.params.id)
    .then(relationship => {
        res.status(200).json(relationship.description)
    })//Sends just the description
})

//Get all relationshiptypes
router.get('/', function (req, res, next) {
    knex('relationship_types').select()
    .then(relationships => {
        console.log("Get relationshiptypes:", relationships)
        res.status(200).json(relationships)
    })
})
module.exports = router;