# LivingLegacy Backend #

## Installation ##

1. [Install Node](https://nodejs.org/en/download/)
2. [Install NPM](https://www.npmjs.com/get-npm)
3. [Install Git](https://git-scm.com/download/win)
4. [Install Postgress](https://www.postgresql.org/download/windows/)
    - This is the database software

## Command Prompt ##

5. Open Command Prompt (cmd)
6. [cd](https://www.digitalcitizen.life/command-prompt-how-use-basic-commands) into the directory you would like the repository on your computer
7. Run `git clone https://AndrewCharlesHay@bitbucket.org/AndrewCharlesHay/livinglegacybackend.git`
    - This should download the files to your computer
8. Run `cd livinglegacybackend`
    - This should take you to the project folder
9. Run `npm install`
    - This should install all the dependencies 
10. Run `createdb`
    - Creates the database without a schema
11. Run `npx knex migrate:latest`
    - Creates the schema I wrote
12. Run `npx knex seed:run`
    - This puts all the dummy data in
13. Run `node app.js`
    - This runs the database
    -You should be all set with the backend now